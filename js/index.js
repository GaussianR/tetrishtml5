var Tetris = 
{
  m_Level: 0,
  m_Score: 0,
  m_Width: 10,
  m_Height: 18,
  m_Interval: 0,
  m_Active: null,
  m_TokenStep: 0,
  m_NextToken: 0,
  m_ScoreBase: 10,
  m_Container: [],
  m_LinesQueue: [],
  m_CurrentHeight: 0,
  m_LinesCompleted: 0,
  m_ScoreLevelExp: 1.1,
  m_IntervalLevelExp: 1.0/1.025,
  m_Patterns:
  [
    { 
      m_Color: 'orange',
      m_Dimensions:
      {
        m: 1,
        n: 4
      },
      m_Container: 
      [
        [1, 1, 1, 1]
      ],
      m_Center: {i: 0, j: 1},
      m_Position: null
    },
    { 
      m_Color: 'red',
      m_Dimensions:
      {
        m: 2,
        n: 2
      },
      m_Container: 
      [
        [1, 1],
        [1, 1]
      ],
      m_Center: {i: 1, j: 1},
      m_Position: null
    },
    { 
      m_Color: 'yellow',
      m_Dimensions:
      {
        m: 2,
        n: 3
      },
      m_Container: 
      [
        [1, 1, 1],
        [0, 1, 0]
      ],
      m_Center: {i: 0, j: 1},
      m_Position: null
    },
    { 
      m_Color: 'cyan',
      m_Dimensions:
      {
        m: 2,
        n: 3
      },
      m_Container: 
      [
        [0, 1, 1],
        [1, 1, 0]
      ],
      m_Center: {i: 0, j: 1},
      m_Position: null
    },
    { 
      m_Color: 'greenyellow',
      m_Dimensions:
      {
        m: 2,
        n: 3
      },
      m_Container: 
      [
        [1, 1, 0],
        [0, 1, 1]
      ],
      m_Center: {i: 0, j: 1},
      m_Position: null
    },
    {
      m_Color: 'magenta',
      m_Dimensions:
      {
        m: 2,
        n: 3
      },
      m_Container: 
      [
        [1, 1, 1],
        [0, 0, 1]
      ],
      m_Center: {i: 0, j: 0},
      m_Position: null
    },
    {
      m_Color: 'blue',
      m_Dimensions:
      {
        m: 2,
        n: 3
      },
      m_Container: 
      [
        [1, 1, 1],
        [1, 0, 0]
      ],
      m_Center: {i: 0, j: 0},
      m_Position: null
    }
  ],
  Init: function( )
  {
    var wrapper = $('#game-wrapper');
    for(var i = 0; i < this.m_Height; ++i)
    {
      this.m_Container[i] = [];
      var row = $('<div class="row"></div>');
      
      wrapper.append(row);
      for(var j = 0; j < this.m_Width; ++j)
      {
        var object = $('<div class="game-item"></div>');
        row.append(object);
        this.m_Container[i][j] =
        {
          m_Object: object,
          m_Flag: 0
        };
      }
    }
    
    this.Reset();
  },
  Reset: function( )
  {
    for(var i = 0; i < this.m_Height; ++i)
      for(var j = 0; j < this.m_Width; ++j)
        this.CleanSlot(this.m_Container[i][j]);
    
    $('#game-over').hide();

    this.m_Level = 0;
    this.m_Score = 0;
    this.m_Interval = 400;
    this.m_LinesCompleted = 0;
    this.m_NextToken = Math.floor(Math.random() * this.m_Patterns.length);
    
    this.CreateToken();
    setTimeout(Tetris.Run, Tetris.m_Interval);
    
    this.UpdateLevel();
    this.UpdateLines();
    this.UpdateScore();
    this.UpdateNextToken();
  },
  CreateToken: function( )
  {
    var token = jQuery.extend(true, {}, this.m_Patterns[this.m_NextToken]);
    token.m_Position = {i: token.m_Center.i, j: Math.floor(this.m_Width/2)-1};
    
    if(this.CheckCollision(token))
    {
      this.OnGameOver();
      return;
    }
    
    this.m_NextToken = Math.floor(Math.random() * this.m_Patterns.length);
    this.UpdateNextToken();
    this.m_Active = token;
    this.m_TokenStep = 0;
  },
  ShiftLeftToken: function( )
  {
    var token = jQuery.extend(true, {}, this.m_Active);

    --token.m_Position.j;
    if(!this.CheckCollision(token))
      this.m_Active = token;
  },
  ShiftRightToken: function( )
  {
    var token = jQuery.extend(true, {}, this.m_Active);

    ++token.m_Position.j;
    if(!this.CheckCollision(token))
      this.m_Active = token;
  },
  ShiftDownToken: function( )
  {
    var token = jQuery.extend(true, {}, this.m_Active);

    ++token.m_Position.i;
    if(!this.CheckCollision(token))
      this.m_Active = token;
  },
  RotateToken: function( )
  {
    var active = this.m_Active;
    var token = jQuery.extend(true, {}, this.m_Active);

    token.m_Dimensions.m = active.m_Dimensions.n;
    token.m_Dimensions.n = active.m_Dimensions.m;
    
    token.m_Container = [];
    for(var i = 0; i < token.m_Dimensions.m; ++i)
    {
      token.m_Container[i] = [];
      for(var j = 0; j < token.m_Dimensions.n; ++j)
      {
        var k = token.m_Dimensions.n-j-1;
        token.m_Container[i][j] = active.m_Container[k][i];
      }
    }
    
    if(!this.CheckCollision(token))
      this.m_Active = token;
  },
  RenderToken: function( fn )
  {
    var active = this.m_Active;
    var pos = active.m_Position;
    var center = active.m_Center;
    var dim = active.m_Dimensions;    
    var li = pos.i - center.i, ui = li + dim.m;
    var lj = pos.j - center.j, uj = lj + dim.n;
    for(var j = lj, x = 0; j < uj; ++j, ++x)
      for(var i = li, y = 0; i < ui; ++i, ++y)
        if(active.m_Container[y][x])
          fn(this.m_Container[i][j], active.m_Color);
    
    return true;
  },
  CleanSlot: function( item )
  {
    item.m_Flag = false;
    item.m_Object.removeClass('game-item-active');
    item.m_Object.css('background-image', 'none');
    item.m_Object.css('transform', 'rotate(0deg)');
    item.m_Object.css('-webkit-transform', 'rotate(0deg)');
  },
  CleanToken: function( )
  {
    this.RenderToken(this.CleanSlot);
  },
  DrawSlot: function( item, color )
  {
    item.m_Flag = true;
    item.m_Object.addClass('game-item-active');
    item.m_Object.css('transform', 'rotate(0deg)');
    item.m_Object.css('-webkit-transform', 'rotate(0deg)');
    item.m_Object.css('background-image', 'url(img/' + color + '.png)');
  },
  DrawSlotImg: function( item, item_old )
  {
    
  },
  DrawToken: function( )
  {
    if( this.m_Active == null )
      return;

    this.RenderToken(this.DrawSlot);
  },
  UpdateLines: function( )
  {
    $('#game-lines').text(this.m_LinesCompleted);
  },
  UpdateLevel: function( )
  {
    $('#game-level').text((this.m_Level+1));
  },
  UpdateScore: function( )
  {
    $('#game-score').text(this.m_Score);
  },
  UpdateNextToken: function( )
  {
    $('#game-next-token').attr('src', 'img/token_' + this.m_Patterns[this.m_NextToken].m_Color + '.png');
  },
  OnLineComplete: function( line )
  {
    for(var i = line-1; i >= 0; --i)
    {
      for(var j = 0; j < this.m_Width; ++j)
      {
        var upper_item = this.m_Container[i][j];
        var lower_item = this.m_Container[i+1][j];
        
        lower_item.m_Flag = upper_item.m_Flag;
        lower_item.m_Object.attr('style', upper_item.m_Object.attr('style'));
        lower_item.m_Object.attr('class', upper_item.m_Object.attr('class'));
        lower_item.m_Object.addClass('game-item-' + (i+1) + '-' + j);
        lower_item.m_Object.removeClass('game-item-' + i + '-' + j);
      }
    }
    
    for(var j = 0; j < this.m_Width; ++j)
      this.CleanSlot(this.m_Container[0][j]);

    if(this.m_Active != null)
      ++this.m_Active.m_Position.i;
    
    if(!(++this.m_LinesCompleted % 9))
    {
      ++this.m_Level;
      this.UpdateLevel();
      this.m_Interval*=this.m_IntervalLevelExp; 
    }
    
    this.UpdateLines();
    this.m_Score = Math.floor(this.m_Score + this.m_ScoreBase*Math.pow(this.m_ScoreLevelExp, this.m_Level));
    this.UpdateScore();
  },
  CompleteLine: function( i )
  {
    var count = 0;
    this.m_Container[i][0].m_Object.parent().children().animate({borderSpacing: -90}, 
    {
      step: function(now, fx) 
      {
        $(this).css('-webkit-transform', 'rotate(' + now + 'deg)');
        $(this).css('-moz-transform', 'rotate(' + now + 'deg)');
        $(this).css('transform', 'rotate(' + now + 'deg)');
      },
      duration: 'fast',
      complete: function()
      {
        if(++count == Tetris.m_Width)
          Tetris.m_LinesQueue.push(i);
      }
    }, 'linear');
  },
  CheckLines: function( )
  {
    for(var i = 0; i < this.m_Height; ++i)
    {
      var completed = true;
      for(var j = 0; j < this.m_Width; ++j)
      {
        if(this.m_Container[i][j].m_Flag != true)
          completed = false;
      }
      
      if(completed)
        this.CompleteLine(i);
    }
  },
  CheckCollision: function( token )
  {
    var pos = token.m_Position;
    var center = token.m_Center;
    var dim = token.m_Dimensions;
    var li = pos.i - center.i, ui = li + dim.m;
    var lj = pos.j - center.j, uj = lj + dim.n;
    if( ui > this.m_Height || lj < 0 || uj > this.m_Width )
      return true;
    
    for(var j = lj, x = 0; j < uj; ++j, ++x)
      for(var i = li, y = 0; i < ui; ++i, ++y)
        if(token.m_Container[y][x] && this.m_Container[i][j].m_Flag)
          return true;
    
    return false;
  },
  CheckMovCollision: function()
  {
    var token = jQuery.extend(true, {}, this.m_Active);
    ++token.m_Position.i;

    if(this.CheckCollision(token))
    {
      this.DrawToken();
      this.CheckLines();
      this.CreateToken();
      return true;
    }
    
    return false;
  },
  CheckLinesQueue: function( )
  {
    while(this.m_LinesQueue.length)
      this.OnLineComplete(this.m_LinesQueue.shift());
  },
  InteractWithToken: function( idx )
  {
    if(this.m_Active == null)
      return;
    
    this.CheckLinesQueue();
    this.CleanToken();
    
    switch(idx)
    {
      case 1: this.RotateToken(); break;
      case 2: this.ShiftLeftToken(); break;
      case 3: this.ShiftRightToken(); break;
      case 4: this.ShiftDownToken(); break;
    }
    
    if(!this.CheckMovCollision())
      this.DrawToken();
  },
  ProcessToken: function( )
  {
    if(this.m_Active == null)
      return false;
    
    this.CheckLinesQueue();
    if(this.m_TokenStep > 0)
    {
      this.CleanToken();
      if(!this.CheckMovCollision())
        ++this.m_Active.m_Position.i;
    }

    this.DrawToken();
    ++this.m_TokenStep;
    return true;
  },
  OnGameOver: function( )
  {
    this.m_Active = null;
    $('#game-over').show("slow", function(){});
  },
  Notify: function( val )
  {
    $('#notify').text(val);
  },
  Run: function( )
  {
    if(Tetris.ProcessToken())
      setTimeout(Tetris.Run, Tetris.m_Interval);
  }
};

$(document).ready(function()
{
  Tetris.Init();
});

$(document).keydown(function(e){
    switch (e.keyCode) 
    { 
      case 37:
        Tetris.InteractWithToken(2);
      break;
      case 38:
        Tetris.InteractWithToken(1);
      break;
      case 39:
        Tetris.InteractWithToken(3);
      break;
      case 40:
        Tetris.InteractWithToken(4);
      break;
    }
});

$('#game-restart').on('click', function()
{
  Tetris.Reset();
});